### Overview 
This is an POC approach to:
1. collect MQTT data from a physical device (Tinkerforge temperature, humidity, ambient-light)
2. publish collected MQTT data to a Kubernetes cluster service (MQTT Broker)
3. Monitoring metrics and incoming MQTT data to a metrics endpoint
4. within the Kubernetes cluster publish MQTT data over Nats JetStream message bus
5. subscribe data from message bus and provide another metrics endpoint

#### work/data flow
1. weather data being collected by Tinkerforge Bricklet and requested by *climate-mqtt-pb*
2. *climate-mqtt-pub* also publishes data to MQTT Broker
3. *climate-mqtt-sub* consumes from MQTT Broker and republishes data to Nats JetStream
4. *climate-stream-sub* consumes data from Nats JetStream message bus

### Getting started
##### MQTT publisher
##### Basic NestJS dev environment dependencies
* ```nix-shell default.nix``` 
* ```nest new climate-mqtt-pub```
* ```cd climate-mqtt-pub```
* ```npm install tinkerforge```
* ```npm i --save mqtt```
* ```npm i --save @nestjs/microservices```
* ```npm install --save @nestjs/terminus``` <- metrics
* ```npm install prom-client``` <- prometheus 
* ```npm install --save hbs``` <- to render html templates
* ```npm i --save @nestjs/config```
* ```npm i log-timestamp```

get tinkerforge shell binding from website: [https://www.tinkerforge.com/en/doc/Software/API_Bindings_Shell.html#api-bindings-shell]
get Bricklet UIDs: ```tinkerforge enumerate``` 

##### Local testing:
* mosquitto cli must be installed: 
* ```mosquitto -v```
* ```mosquitto_sub -h localhost -t weather-data```


##### MQTT subscripber

##### Local testing:
* mosquitto cli must be installed: 
* ```mosquitto -v```
* ```mosquitto_pub -h localhost -t test -m "published some text ..."```

##### Monitoring endpoints
```http://localhost:3016/metrics```
```http://localhost:3017/metrics```

##### Nats JetStream dependencies
```npm i --save nats```
```npm install @alexy4744/nestjs-nats-jetstream-transporter nats@latest```

##### Capnproto protocoll buffer dependencies
```npm install --save capnp-ts```
```npm install capnpc-ts```
* generate capnproto unique id: ```capnp id```
* compiling it by point capnpc to capnpc-ts: ```capnpc -o node_modules/.bin/capnpc-ts src/capnproto/weahterdata.capnp```

##### Troubleshooting
* somehow the nats client ```emit()``` function does not accept protocol buffers respectively ArrayBuffer type so conversion from ArrayBuffer to typed Array (Uint8Array) is necessary. But then all the entries of the array are on the bus which means a JSON like key value style, so conversion to Array<number> is the next step. Then, in the consumer all the way back to finally get all the values out of capnproto.