import { Injectable, Logger } from '@nestjs/common';
import { BrickletConnectionIf, BrickletIf } from './app.interface';
import { AppConnectionHelper } from './app.connection.helper';

const HOST: string = 'localhost';
const PORT: number = 4223;
const UID: string = 'iaX';

@Injectable()
export class AppAmbService implements BrickletConnectionIf, BrickletIf {
  private logger = new Logger('AppAmbService');
  connection: any;
  brickletType: any;
  lib: any;

  constructor(private readonly appConnectionHelper: AppConnectionHelper) {}

  public setConnectionParams(): BrickletIf {
    let [Tinkerforge, ipcon] = this.appConnectionHelper.connectionParams();
    let amb = new Tinkerforge.BrickletAmbientLight(UID, ipcon);
    let bricklet: BrickletIf = {
      connection: ipcon,
      brickletType: amb,
      lib: Tinkerforge,
    };

    return bricklet;
  }

  public ambientLightCallback(): Promise<number> {
    let bricklet: BrickletIf = this.setConnectionParams();
    this.appConnectionHelper.establishConnection(HOST, PORT, bricklet);

    bricklet.connection.on(
      bricklet.lib.IPConnection.CALLBACK_CONNECTED, () => {
        bricklet.brickletType.setIlluminanceCallbackPeriod(5000);
      },
    );

    return new Promise((resolve, reject) => {
      bricklet.brickletType.on(bricklet.lib.BrickletAmbientLight.CALLBACK_ILLUMINANCE, (illuminance: number) => {
        illuminance = illuminance / 10.0;
        this.logger.log('Illuminance: ' + illuminance + ' lx');
        resolve(illuminance);
      });
    });
  }
}
