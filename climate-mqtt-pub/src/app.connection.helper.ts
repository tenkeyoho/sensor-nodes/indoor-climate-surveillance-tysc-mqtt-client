import { Injectable, Logger } from '@nestjs/common';
import { BrickletIf } from './app.interface';
import Tinkerforge = require('tinkerforge');

@Injectable()
export class AppConnectionHelper {
  private logger = new Logger('AppConnectionHelper');

  public connectionParams(): Array<any> {
    let ipcon = new Tinkerforge.IPConnection();
    return [Tinkerforge, ipcon];
  }

  public establishConnection(host: string, port: number, bricklet: BrickletIf): void {
    bricklet.connection.connect(host, port, (error: string) => {
      this.logger.error('Error: ' + error);
      bricklet.connection.disconnect();
      this.logger.warn('Bricklet: ' + bricklet.brickletType + ' disconnected');
    });
  }
}
