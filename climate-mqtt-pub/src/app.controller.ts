import {
  Controller,
  Post,
  Logger,
} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  private logger = new Logger('AppController');

  constructor(private readonly appService: AppService) {}

  @Post('add')
    public async sendMessage() {
    this.logger.log('MQTT published by controller via POST');
    return this.appService.senAnotherMessage('string-based-data');
  }
}
