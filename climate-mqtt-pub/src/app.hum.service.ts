import { Injectable, Logger } from '@nestjs/common';
import { BrickletConnectionIf, BrickletIf } from './app.interface';
import { AppConnectionHelper } from './app.connection.helper';

const HOST: string = 'localhost';
const PORT: number = 4223;
const UID: string = 'fNX';

@Injectable()
export class AppHumService implements BrickletConnectionIf, BrickletIf {
  private logger = new Logger('AppHumService');
  connection: any;
  brickletType: any;
  lib: any;

  constructor(private readonly appConnectionHelper: AppConnectionHelper) {}

  public setConnectionParams(): BrickletIf {
    let [Tinkerforge, ipcon] = this.appConnectionHelper.connectionParams();
    let hum = new Tinkerforge.BrickletHumidity(UID, ipcon);
    let bricklet: BrickletIf = {
      connection: ipcon,
      brickletType: hum,
      lib: Tinkerforge,
    };

    return bricklet;
  }

  public humidityCallback(): Promise<number> {
    let bricklet: BrickletIf = this.setConnectionParams();
    this.appConnectionHelper.establishConnection(HOST, PORT, bricklet);

    bricklet.connection.on(
      bricklet.lib.IPConnection.CALLBACK_CONNECTED, () => {
        bricklet.brickletType.setHumidityCallbackPeriod(5000);
      },
    );

    return new Promise((resolve, reject) => {
      bricklet.brickletType.on(bricklet.lib.BrickletHumidity.CALLBACK_HUMIDITY, (humidity: number) => {
        humidity = humidity / 10.0;
        this.logger.log('Humidity: ' + humidity + ' %RH');
        resolve(humidity);
      });
    });
  }
}
