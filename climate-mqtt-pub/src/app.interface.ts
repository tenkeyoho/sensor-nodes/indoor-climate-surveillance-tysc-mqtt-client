export interface BrickletConnectionIf {
  setConnectionParams(): BrickletIf;
  brickletCallback?: () => number;
}

export interface BrickletIf {
  connection: any;
  brickletType: any;
  lib: any;
}
