import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AppTempService } from './app.temp.service';
import { AppHumService } from './app.hum.service';
import { AppAmbService } from './app.amb.service';
import { AppConnectionHelper } from './app.connection.helper';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [
    AppService,
    AppTempService,
    AppHumService,
    AppAmbService,
    AppConnectionHelper,
  ],
})
export class AppModule {}
