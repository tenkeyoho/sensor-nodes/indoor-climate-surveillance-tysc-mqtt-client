import { Injectable, Logger } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, MessagePattern } from '@nestjs/microservices';
import { AppTempService } from './app.temp.service';
import { AppHumService } from './app.hum.service';
import { AppAmbService } from './app.amb.service';

@Injectable()
export class AppService {
  private logger = new Logger('AppService');
  private client: ClientProxy;

  constructor(
    private readonly appTempService: AppTempService,
    private readonly appHumService: AppHumService,
    private readonly appAmbService: AppAmbService,
  ) {
    this.client = ClientProxyFactory.create({
      transport: 3,
      options: {
        url: 'mqtt://mqtt.cluster.tenkeyoho.cloud:1883',  //'mqtt://localhost:1883',
        qos: 2,
      },
    });
  }

  combineBrickletValues(): void {
    let temperature, humidity, illuminance: number;    
    setInterval(() => {
      Promise.all([
        this.appTempService.temperatureCallback(), 
        this.appHumService.humidityCallback(), 
        this.appAmbService.ambientLightCallback()
      ]).then((resolve) => {
          [temperature, humidity, illuminance] = resolve;
          this.sendMessage(resolve);
      }).catch((error) => this.logger.error(error.message));
    }, 10000)
  }

  private convertArrayToJson(arr: Array<number>) {
    let arrToString = JSON.stringify(Object.assign({}, arr));
    return JSON.parse(arrToString);
  }

  private sendMessage(data: Array<number>) {
    let emitData = this.convertArrayToJson(data);
    this.client.emit('wrzbrg-weather-data', emitData);
    this.logger.log('emitted weather data: ' + data.toLocaleString());
  }

  @MessagePattern("test")
  public senAnotherMessage(data: string) {
    this.client.emit('test', 'some data');
  }
}
