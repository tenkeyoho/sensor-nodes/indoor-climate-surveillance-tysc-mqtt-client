import { Injectable, Logger } from '@nestjs/common';
import { BrickletConnectionIf, BrickletIf } from './app.interface';
import { AppConnectionHelper } from './app.connection.helper';

const HOST: string = 'localhost';
const PORT: number = 4223;
const UID: string = 'dX7';

@Injectable()
export class AppTempService implements BrickletConnectionIf, BrickletIf {
  private logger = new Logger('AppTempService');
  connection: any;
  brickletType: any;
  lib: any;

  constructor(private readonly appConnectionHelper: AppConnectionHelper) {}

  public setConnectionParams(): BrickletIf {
    let [Tinkerforge, ipcon] = this.appConnectionHelper.connectionParams();
    let temp = new Tinkerforge.BrickletTemperature(UID, ipcon);
    let bricklet: BrickletIf = {
      connection: ipcon,
      brickletType: temp,
      lib: Tinkerforge,
    };

    return bricklet;
  }

  public temperatureCallback(): Promise<number> {
    let bricklet: BrickletIf = this.setConnectionParams();
    this.appConnectionHelper.establishConnection(HOST, PORT, bricklet);

    bricklet.connection.on(
      bricklet.lib.IPConnection.CALLBACK_CONNECTED, () => {
        bricklet.brickletType.setTemperatureCallbackPeriod(5000);
      },
    );

    return new Promise((resolve, reject) => {
      bricklet.brickletType.on(bricklet.lib.BrickletTemperature.CALLBACK_TEMPERATURE, (temperature: number) => {
          temperature = temperature / 100.0;
          this.logger.log('Temperature: ' + temperature + ' °C');
          resolve(temperature);
        });
    });
  }
}
