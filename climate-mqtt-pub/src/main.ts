import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AppService } from './app.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.get(AppService).combineBrickletValues();
  await app.listen(3015);
}
bootstrap();
