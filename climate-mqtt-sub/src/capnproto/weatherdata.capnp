@0x888371cb0d671539;
struct ClimateData {
    timestamp @0 :UInt32;
    location @1 :Text;
    temperature @2 :Float32;
    humidity @3 :Float32;
    illuminance @4 :Float32;
}
