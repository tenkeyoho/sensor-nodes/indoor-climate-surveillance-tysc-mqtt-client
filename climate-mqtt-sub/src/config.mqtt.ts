import { MqttOptions, Transport } from '@nestjs/microservices';

export const mqttConfig: MqttOptions = {
    transport: Transport.MQTT,
    options: {
        url: 'mqtt://mqtt.cluster.tenkeyoho.cloud:1883', //process.env.MESSAGE_BROKER_URL || 'mqtt://mqtt.cluster.tenkeyoho.cloud:1883', //'mqtt://localhost:1883',
    }
};