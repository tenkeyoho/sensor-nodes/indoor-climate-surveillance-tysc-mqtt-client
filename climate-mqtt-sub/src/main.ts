import { NestFactory } from '@nestjs/core';
import { natsStreamConfig } from "./config.nats";
import { mqttConfig } from "./config.mqtt";
import { MqttModule } from 'mqtt/mqtt.module';

async function bootstrap() {
  const app = await NestFactory.create(MqttModule);
  app.connectMicroservice(mqttConfig)
  app.connectMicroservice(natsStreamConfig);
  app.startAllMicroservices();

  await app.listenAsync(3016);
}
bootstrap();

