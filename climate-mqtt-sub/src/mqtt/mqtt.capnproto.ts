import { Injectable } from "@nestjs/common";
import * as capnp from "capnp-ts";
import { ClimateData } from "@capn/weatherdata.capnp";

@Injectable()
export class MqttCapnproto{

    public serialize(data: number[]): ArrayBuffer {
        const message = new capnp.Message();
        const climateData = message.initRoot(ClimateData);
        
        climateData.setTimestamp(Math.floor(Date.now()/1000));
        climateData.setLocation('Wuerzburg');
        climateData.setTemperature(data[0]);
        climateData.setHumidity(data[1]);
        climateData.setIlluminance(data[2]);

        return message.toArrayBuffer();
    }

    public deserialize(data: ArrayBuffer): void {
        const message = new capnp.Message(data, false, false);
        console.log(
            message.getRoot(ClimateData).getTimestamp(),
            message.getRoot(ClimateData).getLocation(),
            message.getRoot(ClimateData).getTemperature(), 
            message.getRoot(ClimateData).getHumidity(), 
            message.getRoot(ClimateData).getIlluminance()
            );
    }
}