import {Controller, Logger} from '@nestjs/common';
import { Ctx, MessagePattern, MqttContext, Payload, Transport } from '@nestjs/microservices';
import { MqttService } from './mqtt.service';
import { NatsClient } from '@alexy4744/nestjs-nats-jetstream-transporter';

@Controller('mqtt')
export class MqttController {
    [x: string]: any;
    private logger = new Logger('MqttController');
    private client = new NatsClient();

    constructor(
        private mqttService: MqttService) {}

    @MessagePattern('wrzbrg-weather-data', Transport.MQTT)
    public async subscribeMqtt(@Payload() data: number[], @Ctx() context: MqttContext): Promise<void> {
        this.mqttService.subscribe(data, context);
        this.createMessagePayload(data);
        this.logger.log('Emitted weather data to JetStream');
    }

    // TODO: hier noch ein die Convertierung von Arraybuffer nach Uint8Array in ein Callback auslagern.
    public createMessagePayload(data: number[]): void {
        // The toArrayBuffer() method from Capnproto is converting an Uint8Array to its buffer.
        // ArrayBuffer is an object which cannot be read or written to so convert it to Uint8Array.
        const weatherdata: ArrayBuffer = this.mqttService.create(data);

        // Uint8Array is an Object similiar to an array ...
        const payload = new Uint8Array(weatherdata, 0, weatherdata.byteLength) 

        // ... to prevent sending an object over message bus the Uint8Array data is converted to Array<number> 
        this.client.emit('weatherdata', [].slice.call(payload));
    }
}
