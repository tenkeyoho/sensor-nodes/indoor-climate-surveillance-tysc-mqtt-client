import { Module } from '@nestjs/common';
import { MqttService } from './mqtt.service';
import { MqttController } from './mqtt.controller';
import { MqttCapnproto } from './mqtt.capnproto';
import { PrometheusModule } from 'prometheus/prometheus.module';
import { MetricsModule } from 'metrics/metrics.module';

@Module({
  imports: [PrometheusModule, MetricsModule],
  providers: [MqttService, MqttCapnproto],
  controllers: [MqttController],
  exports: [ MqttService ]
})
export class MqttModule {}
