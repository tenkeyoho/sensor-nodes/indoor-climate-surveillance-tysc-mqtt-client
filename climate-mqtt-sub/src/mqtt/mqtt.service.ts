import {Injectable, Logger} from '@nestjs/common';
import { MqttContext } from '@nestjs/microservices';
import { PrometheusService } from 'prometheus/prometheus.service';
import {MqttCapnproto} from "./mqtt.capnproto";

@Injectable()
export class MqttService {
    constructor(
        private mqttcapn: MqttCapnproto, 
        private promClientService: PrometheusService,
        ) {}

    private logger = new Logger('MqttService');

    public get isConnected(): boolean {
        return true;
    }

    public subscribe(data: number[], context: MqttContext): void {
        this.logger.log(`Subscribe topic: "${context.getTopic()}" with data: `);
        console.log(data);
        const gaugeTemp = this.promClientService.registerGauge("MQTT_SERVICE_TEMP", "temperature_gauge");
        gaugeTemp.set(data[0]);
        
        const gaugeHum = this.promClientService.registerGauge("MQTT_SERVICE_HUM", "humidity_gauge");
        gaugeHum.set(data[1]);
    
        const gaugeIllum = this.promClientService.registerGauge("MQTT_SERVICE_ILLUM", "illuminance_gauge");
        gaugeIllum.set(data[2]);
    }

    public create(data: number[]): ArrayBuffer {
        this.logger.log('create emittable JetStream message');
        this.mqttcapn.deserialize(this.mqttcapn.serialize(data));
        return this.mqttcapn.serialize(data);
    }
}
