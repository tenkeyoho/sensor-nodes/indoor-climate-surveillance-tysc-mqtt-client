"use strict";
/* tslint:disable */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClimateData = exports._capnpFileId = void 0;
const capnp_ts_1 = require("capnp-ts");
exports._capnpFileId = "888371cb0d671539";
class ClimateData extends capnp_ts_1.Struct {
    getTimestamp() { return capnp_ts_1.Struct.getUint32(0, this); }
    setTimestamp(value) { capnp_ts_1.Struct.setUint32(0, value, this); }
    getLocation() { return capnp_ts_1.Struct.getText(0, this); }
    setLocation(value) { capnp_ts_1.Struct.setText(0, value, this); }
    getTemperature() { return capnp_ts_1.Struct.getFloat32(4, this); }
    setTemperature(value) { capnp_ts_1.Struct.setFloat32(4, value, this); }
    getHumidity() { return capnp_ts_1.Struct.getFloat32(8, this); }
    setHumidity(value) { capnp_ts_1.Struct.setFloat32(8, value, this); }
    getIlluminance() { return capnp_ts_1.Struct.getFloat32(12, this); }
    setIlluminance(value) { capnp_ts_1.Struct.setFloat32(12, value, this); }
    toString() { return "ClimateData_" + super.toString(); }
}
exports.ClimateData = ClimateData;
ClimateData._capnp = { displayName: "ClimateData", id: "c2631cc0511f4975", size: new capnp_ts_1.ObjectSize(16, 1) };
