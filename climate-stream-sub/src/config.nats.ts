import { NatsTransportStrategy } from '@alexy4744/nestjs-nats-jetstream-transporter';

export const natsConsumerConfig = {
    strategy: new NatsTransportStrategy({
        consumer: (options) => {
            options.durable('climate-durable-connection')
            options.deliverTo('climate-queue-group');
            options.queue('climate-queue-group')
        },
        connection: {
            servers: ["nats://localhost:4222"]
        },
        queue: 'climate-queue-group',
        streams: [
            {
                name: 'datastream',
                subjects: [ 'weatherdata' ]
            }
        ],
        onError: (message) => message.ack()
    })
};