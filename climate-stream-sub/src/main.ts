import { NestFactory } from '@nestjs/core';
import { natsConsumerConfig } from "./config.nats";
import { StreamModule } from './stream/stream.module';

async function bootstrap() {
  const app = await NestFactory.create(StreamModule);
  app.connectMicroservice(natsConsumerConfig);
  
  app.startAllMicroservices()

  await app.listenAsync(3017);
}
bootstrap();
