import { Controller, Logger } from '@nestjs/common';
import { Ctx, EventPattern, NatsContext, Payload } from '@nestjs/microservices';
import { StreamService } from './stream.service';

@Controller('stream')
export class StreamController {
    private logger = new Logger('StreamController');

    constructor(private readonly streamService: StreamService) {}

    @EventPattern('weatherdata')
    public getNotifications(@Payload() data: Array<number>, @Ctx() context: NatsContext): void { //Array<number>
        require('log-timestamp');
        console.log(`Subscribed message subject: ${context.getSubject()}`);    
        //this.streamService.monitor(data);
        this.streamService.deserialize(data);
    }
}
