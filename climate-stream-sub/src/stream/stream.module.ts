import { Module } from '@nestjs/common';
import { StreamService } from './stream.service';
import { StreamController } from './stream.controller';
import { MetricsModule } from 'src/metrics/metrics.module';
import { PrometheusModule } from 'src/prometheus/prometheus.module';

@Module({
  imports: [PrometheusModule, MetricsModule],
  providers: [StreamService],
  controllers: [StreamController],
  exports: [ StreamService ]
})
export class StreamModule {}
