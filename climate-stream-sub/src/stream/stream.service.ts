import {Injectable, Logger} from '@nestjs/common';
import { PrometheusService } from 'src/prometheus/prometheus.service';
import * as capnp from "capnp-ts";
import { ClimateData } from 'src/capnproto/weatherdata.capnp';

@Injectable()
export class StreamService {
    private logger = new Logger('StreamService');

    constructor( private promClientService: PrometheusService ) {}

    public get isConnected(): boolean {
        return true;
    }

    public monitor(data: Array<number>): void {        
        const gaugeTimestamp = this.promClientService.registerGauge("STREAM_SERVICE_TIME", "time_gauge");
        const gaugeTemp = this.promClientService.registerGauge("STREAM_SERVICE_TEMP", "temperature_gauge");
        const gaugeHum = this.promClientService.registerGauge("STREAM_SERVICE_HUM", "humidity_gauge");
        const gaugeIllum = this.promClientService.registerGauge("STREAM_SERVICE_ILLUM", "illuminance_gauge");
        // gaugeTimestamp.set(message.getRoot(ClimateData).getTimestamp());
        // gaugeTemp.set(message.getRoot(ClimateData).getTemperature());      
        // gaugeHum.set(message.getRoot(ClimateData).getHumidity());
        // gaugeIllum.set(message.getRoot(ClimateData).getIlluminance());
    }

    public deserialize(data: Array<number>): void {
        const typedArr = Uint8Array.from(data);
        const message = new capnp.Message(typedArr.buffer, false, false);
        console.log(
            message.getRoot(ClimateData).getTimestamp(),
            message.getRoot(ClimateData).getLocation(),
            message.getRoot(ClimateData).getTemperature(), 
            message.getRoot(ClimateData).getHumidity(), 
            message.getRoot(ClimateData).getIlluminance()
        );
    }
}
